xmpp-rs
=======

What's this?
------------

A very much WIP rust XMPP library with the goals of being type-safe, well-tested and simple.

Contact
-------

There is an XMPP MUC for the discussion of this library, feel free to join! :)

[xmpp-rs@muc.linkmauve.fr](xmpp:xmpp-rs@muc.linkmauve.fr?join)

Can I use it yet?
-----------------

No, there's still a lot of work that needs to be done before this could be used for anything.

I mean, if you really wanted to, sure, but don't expect it to work yet.

What license is it under?
-------------------------

LGPLv3 or later. See `COPYING` and `COPYING.LESSER`.

License yadda yadda.
--------------------

  Copyright 2017, xmpp-rs contributors.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
